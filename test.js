const assert = require('assert');
var Driver = require('./driver.js');
var Taxi = require('./taxi.js');
var Route = require('./route.js');

describe('Driver', function() {
  it("should return  firstName(Luyanda), lastName(Zulu), cellNo(0733054707) and licenseNo(88888)'", function() {
   var driver = new Driver ("Luyanda", "Zulu", 0733054707, 88888);
    assert.equal(driver.firstName, "Luyanda");
    assert.equal(driver.lastName, "Zulu");
    assert.equal(driver.cellNo, 0733054707);
    assert.equal(driver.licenseNo, 88888);

  });
});
describe('Taxi', function(){
  it("should return taxiYear(2007), taxiModel(Siyaya), taxiMake(Toyota) and taxiCapacity(16)'", function(){
    var taxi = new Taxi (2007, "Siyaya", "Toyota", 16);
    assert.equal(taxi.taxiYear,2007);
    assert.equal(taxi.taxiModel,"Siyaya");
    assert.equal(taxi.taxiMake,"Toyota");
    assert.equal(taxi.taxiCapacity,16);
  });
});
describe('Route', function(){
  it("should return startLoc(Khayelitsha) and endLoc(Cape town)'", function(){
    var route = new Route("Khayelitsha", "Cape town");
    assert.equal(route.startLoc,"Khayelitsha");
    assert.equal(route.endLoc,"Cape town");
  })
})
